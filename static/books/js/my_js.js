$(document).ready(function(){

	repopulate_table_with_query("https://www.googleapis.com/books/v1/volumes?q=mirror mirror rwby");

	$("#books_search_button").click(function(){

		var query = $("#books_search_form").val();
		query = "https://www.googleapis.com/books/v1/volumes?q=" + query;
		repopulate_table_with_query(query);
	});

	
});

function repopulate_table_with_query(query) {
	$.ajax({
	      url: query,
	      datatype: "json",
	      beforeSend: function() { // memunculkan lagi search box
	      	$("#books_results").empty();
	      	$("#loading_gif").css("display", "block");
	      },
	      success: function(result){
	      	//console.log(result);
	      	$("#loading_gif").css("display", "none");
	      	var items = result.items;
	        

	        // menambahkan kebali rownya
	        $("#books_results").append(
		        '<div class="row" style="background-color:#802bb1; color:#d1d7e0">' +
	             '<div class="col-3 d-flex justify-content-center h5 py-2 mb-0">Tampilan</div>' +
	             '<div class="col-3 d-flex justify-content-center h5 py-2 mb-0">Judul</div>' +
	             '<div class="col-3 d-flex justify-content-center h5 py-2 mb-0">Penulis</div>' +
	             '<div class="col-3 d-flex justify-content-center h5 py-2 mb-0">Terbit</div>' +
	          	'</div>'
          	)

	        $.each(items, function(index, item) {

	         	   fill_a_row(item); 

	         	   /*var start_time = new Date().getTime();
	         	   var cur_time = start_time;
	         	   while(cur_time - start_time < 1000) {
	         	   		cur_time = new Date().getTime();
	         	   }*/
	         });
	      }
	});
}


function fill_a_row(item) {
		
	  // info sangat jelek. skip buku ini
	  if(item.volumeInfo == undefined) return;

	  // mencari title
	  var title = "-";
	  if(item.volumeInfo.title != undefined) {
	  	title = item.volumeInfo.title;
	  }

	  // mengubah list authors jadi string
	  var authors_as_str = "";
	  if(item.volumeInfo.authors != undefined) {
	 	item.volumeInfo.authors.forEach(name => authors_as_str+=name+", ");
	  	authors_as_str = authors_as_str.slice(0, -2);
	  }
	  else {
	  	authors_as_str = "-";
	  }

	  // mencari image
	  var image = "-";
	  if(item.volumeInfo.imageLinks != undefined
	  	 && item.volumeInfo.imageLinks.thumbnail != undefined) {
	  	image = item.volumeInfo.imageLinks.thumbnail;
	  }
	  
	  // mencari tanggal terbit
	  var publish_date = "-";
	  if(item.volumeInfo.publishedDate != undefined) {
	  	publish_date = item.volumeInfo.publishedDate;
	  }

	  var s = '<div class="row" style="background-color: #caa9e6; color:#2d283e">\n';

	  // special case saat display images jika tidak ada foto
	  if(image == "-") {
	  	s+='<div class="col-3 d-flex justify-content-left py-3">Tidak ada tampilan</div>\n';
	  }
	  else {
	  	s+='<div class="col-3 d-flex justify-content-center py-3 mb">' + '<img src="' + image + '" alt ="no preview found" class="img-fluid img-thumbnail"></div>\n';
	  	
	  }

	  s+= '<div class="col-3 d-flex justify-content-left py-3">' + title + '</div>' + 
	      	'<div class="col-3 d-flex justify-content-center py-3">' + authors_as_str + '</div>' +
	      	'<div class="col-3 d-flex justify-content-center py-3">' + publish_date + '</div>' +
	  	'</div>'


	  $("#books_results").append(s);
	      	
	  
}