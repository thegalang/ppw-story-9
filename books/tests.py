from django.test import TestCase, Client
from django.http import HttpRequest
from pyvirtualdisplay import Display
from . import views
from selenium import webdriver
import time
from datetime import datetime
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.color import Color


class BooksUnitTest(TestCase):

	def setUp(self):
		self.response = Client().get('/books/')

	def test_exist_landing(self):

		self.assertEqual(self.response.status_code, 200)

	def test_exist_greeting(self):

		self.assertContains(self.response, "Mau baca apa hari ini?", count=1, html = True)

	def test_exist_search_form(self):

		self.assertContains(self.response, """id="books_search_form" """, count=1, html = False)


class BooksFunctionalTest(TestCase):

	# set-up and teardown from my story-6
	def setUp(self):

		# global display
		self.display = Display(visible=0, size=(800, 600))
		self.display.start()
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--disable-gpu')
		service_log_path='./chromedriver.log'
		service_args=['--verbose']
		self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)

	def tearDown(self):

		self.display.stop()
		self.browser.quit()

	def test_search_is_working(self):

		self.browser.get('localhost:8000/books/')
		time.sleep(2)

		# mencari dan mengisi searchbox
		form = self.browser.find_element_by_id('books_search_form')
		form.send_keys('Red Like Roses')
		time.sleep(2)

		btn = self.browser.find_element_by_id('books_search_button')
		btn.click()
		time.sleep(10)

		self.assertIn("Monty Oum", self.browser.page_source)

	def test_exist_content_on_open(self):

		self.browser.get('localhost:8000/books/')
		time.sleep(10)

		# default query saya "mirror mirror RWBY. Cari apakah ada sebagai html"
		self.assertIn( "RWBY: Official Manga Anthology, Vol. 2", self.browser.page_source)

	def test_exist_loading_gif_on_open_works(self):

		self.browser.get('localhost:8000/books/')
		loading_gif = self.browser.find_element_by_id('loading_gif')
		self.assertEqual(loading_gif.value_of_css_property('display'), 'block')

		time.sleep(10)

		self.assertEqual(loading_gif.value_of_css_property('display'), 'none')

	def test_exist_loading_gif_on_search(self):

		self.browser.get('localhost:8000/books/')
		loading_gif = self.browser.find_element_by_id('loading_gif')

		# saat mengirim query, loadingnya aktif
		form = self.browser.find_element_by_id('books_search_form')
		form.send_keys('Red Like Roses')
		time.sleep(1)
		btn = self.browser.find_element_by_id('books_search_button')
		btn.click()
		self.assertEqual(loading_gif.value_of_css_property('display'), 'block')

		time.sleep(10)
		self.assertEqual(loading_gif.value_of_css_property('display'), 'none')

		
