[![pipeline status](https://gitlab.com/thegalang/ppw-story-9/badges/master/pipeline.svg)](https://gitlab.com/thegalang/ppw-story-9/commits/master)
[![coverage report](https://gitlab.com/thegalang/ppw-story-9/badges/master/coverage.svg)](https://gitlab.com/thegalang/ppw-story-9/commits/master)

Author: Galangkangin Gotera, University of Indonesia

Logins:
user: galatea  
Pass: orcust_babel  
  
User: ferguso  
Pass: suka_hal_mudah  
  
User: weiss  
Pass: mirrormirror  
