from django.db import models
from django.contrib.auth.models import User
class User_info(models.Model):

	user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
	status = models.TextField()
	profpic_link = models.CharField(max_length=100)

	def __unicode__(self):
		return self.user.username