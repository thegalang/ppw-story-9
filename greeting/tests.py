from django.test import TestCase, Client
from django.http import HttpRequest
from pyvirtualdisplay import Display
from . import views
from selenium import webdriver
import time
from datetime import datetime
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.color import Color
from django.contrib.auth.models import User
from .models import User_info

class loginUnitTest(TestCase):

	# membuat akun user
	def setUp(self):
		user = User.objects.create(username="galang")
		user.set_password("gotera")
		user.save()
		self.client = Client()

	# login user dengan username dan pass
	def login(self, username, password):
		response = self.client.post("/auth/login/", 
									{'username' : username,
								     'password' : password},
								     follow = True)
		return response

	def test_landing_page_redirects(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 302)
		self.assertRedirects(response, '/greeting/')

	def test_greeting_exist_no_login(self):
		response = Client().get('/greeting/')
		self.assertContains(response, "Halo, pengunjung!", count=1, html = False)

	def test_exist_login_page(self):
		response = Client().get('/auth/login/')
		self.assertEqual(response.status_code, 200)

	def test_username_saving_works(self):
		self.assertEquals(User.objects.count(), 1)
		self.assertEquals(User.objects.filter(username="galang").count(), 1)
		#print(User.objects.filter(username="galang")[0].password)

	def test_invalid_login(self):
		response = self.login("galang", "kangin")
		self.assertFalse(response.context['user'].is_active)

	def test_valid_login(self):

		response = self.client.get('', follow=True)
		self.assertFalse(response.context['user'].is_active)

		response = self.login("galang", "gotera")

		self.assertTrue(response.context['user'].is_active)

	def test_logout_redirects(self):

		response = self.login("galang", "gotera")
		self.assertTrue(response.context['user'].is_active)

		response = self.client.get('/auth/logout/', follow = True)
		self.assertContains(response, "Silahkan login terlebih dahulu :)", count=1, html = False)
		self.assertFalse(response.context['user'].is_active)

	def test_model_saving_works(self):

		usr = User.objects.all()[0]
		usr_info = User_info(user = usr, status="just testing", profpic_link="https://files.catbox.moe/b6eayh.jpg")
		usr_info.save()

		self.assertEquals(User_info.objects.count(), 1)
		self.assertIsInstance(usr_info, User_info)


class loginFunctionalTest(TestCase):

	# set-up and teardown from my story-6
	def setUp(self):

		# global display
		self.display = Display(visible=0, size=(1440, 900))
		self.display.start()
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--disable-gpu')
		service_log_path='./chromedriver.log'
		service_args=['--verbose']
		self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)


	def tearDown(self):

		self.display.stop()
		self.browser.quit()

	def login(self, username, password):
		self.browser.get('localhost:8000/auth/login')
		time.sleep(2)

		username_form = self.browser.find_element_by_id('id_username')
		username_form.send_keys(username)
		time.sleep(0.5)

		password_form = self.browser.find_element_by_id('id_password')
		password_form.send_keys(password)
		time.sleep(0.5)

		submit = self.browser.find_element_by_id('login_button')
		submit.click()
		time.sleep(2)

	def test_invalid_login(self):
		
		self.login("galang", "kangin")

		self.assertIn("Please enter a correct username and password. Note that both fields may be case-sensitive.", self.browser.page_source)

		self.browser.get('localhost:8000')
		time.sleep(2)

		self.assertIn("Silahkan login terlebih dahulu :)", self.browser.page_source)

	def test_valid_login(self):

		self.browser.get('localhost:8000')
		time.sleep(2)
		self.assertIn("Silahkan login terlebih dahulu :)", self.browser.page_source)
		
		self.login("galatea", "orcust_babel")
		self.assertIn("Halo, galatea!", self.browser.page_source)
		self.assertNotIn("Silahkan login terlebih dahulu :)", self.browser.page_source)

	def test_login_logout_button(self):

		self.browser.get('localhost:8000')
		time.sleep(2)

		auth_btn = self.browser.find_element_by_link_text('Login')
		auth_btn.click()
		time.sleep(2)

		# harusnya ada di halaman login. Coba login
		username_form = self.browser.find_element_by_id('id_username')
		username_form.send_keys("galatea")
		time.sleep(0.5)

		password_form = self.browser.find_element_by_id('id_password')
		password_form.send_keys("orcust_babel")
		time.sleep(0.5)

		submit = self.browser.find_element_by_id('login_button')
		submit.click()
		time.sleep(2)

		# harusnya sudah terlogin
		self.assertIn("Halo, galatea!", self.browser.page_source)
		self.assertNotIn("Silahkan login terlebih dahulu :)", self.browser.page_source)
		cur_user = self.browser.find_element_by_id('username-header')
		self.assertEquals(cur_user.text, "galatea")
		time.sleep(0.5)

		# test logout
		auth_btn = self.browser.find_element_by_id('auth_button')
		auth_btn.click()
		time.sleep(2)

		# harusnya kembali ke halaman utama
		self.assertIn("Silahkan login terlebih dahulu :)", self.browser.page_source)
		cur_user = self.browser.find_element_by_id('username-header')
		self.assertEquals(cur_user.text, "pengunjung")
