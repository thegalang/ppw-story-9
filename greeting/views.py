from django.shortcuts import render, HttpResponse, redirect
from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def prepend(arg, prep):
	return prep + arg

def landing_page(request):
	return redirect('greeting:home')

def index(request):
	context = {'app' : 'greeting'}
	return render(request, 'greeting/index.html', context)